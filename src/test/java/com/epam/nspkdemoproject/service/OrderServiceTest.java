package com.epam.nspkdemoproject.service;

import com.epam.nspkdemoproject.model.Order;
import com.epam.nspkdemoproject.repository.OrderRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class OrderServiceTest {

  @Mock
  private OrderRepository orderRepository;
  @Mock
  private Order order;

  @InjectMocks
  private OrderService orderService;

  @Test
  void createOrder() {
    doReturn(order).when(orderRepository).save(order);

    Order actualOrder = orderService.createOrder(order);

    verify(orderRepository).save(order);
    assertEquals(order, actualOrder);
  }
}
