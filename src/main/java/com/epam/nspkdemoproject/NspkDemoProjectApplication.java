package com.epam.nspkdemoproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NspkDemoProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(NspkDemoProjectApplication.class, args);
	}

}
