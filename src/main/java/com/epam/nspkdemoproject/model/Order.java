package com.epam.nspkdemoproject.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "order1")
public class Order implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "order_id")
	private Integer orderId;

	@Column(name = "customer")
	private String customer;

	@Column(name = "barrels")
	private Integer barrels;

	@Column(name = "type")
	private String type;

	@Column(name = "price")
	private Integer price;

	@Column(name = "amount")
	private Integer amount;

	@Column(name = "discount")
	private Integer discount;
}
