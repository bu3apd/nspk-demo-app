package com.epam.nspkdemoproject.service;

import com.epam.nspkdemoproject.exception.ResourceNotFoundException;
import com.epam.nspkdemoproject.model.Order;
import com.epam.nspkdemoproject.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class OrderService {

  private final OrderRepository orderRepository;

  @Transactional
  public Order createOrder(Order order) {
    String type = order.getType();
    Integer barrels = order.getBarrels();
    Integer price = order.getPrice();
    Integer resultPrice;
    int discount;

    if (type.equals("brent")) {
      discount = 5;
      resultPrice = calculateDiscount(barrels, price, discount);
    } else {
      discount = 10;
      resultPrice = calculateDiscount(barrels, price, discount);
    }
    order.setAmount(barrels * price);
    order.setDiscount(resultPrice);
    return orderRepository.save(order);
  }

  private Integer calculateDiscount(Integer barrels, Integer price, Integer discount) {
    int result = 0;
    if (barrels > 100) {
      result = barrels * price;
      result = result / 100 * (100 - discount);
    } else {
      result = barrels * price;
    }
    return result;
  }

  @Transactional
  public Order getOrderById(Integer orderId) {
    return orderRepository.findById(orderId).orElseThrow(() -> new ResourceNotFoundException("Order not found"));
  }
}
