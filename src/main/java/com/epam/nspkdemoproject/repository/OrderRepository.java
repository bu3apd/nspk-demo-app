package com.epam.nspkdemoproject.repository;

import com.epam.nspkdemoproject.model.Order;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<Order, Integer> {
}
