package com.epam.nspkdemoproject.controller;

import com.epam.nspkdemoproject.dto.OrderDto;
import com.epam.nspkdemoproject.dto.OrderResponseDto;
import com.epam.nspkdemoproject.model.Order;
import com.epam.nspkdemoproject.service.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/order")
@RequiredArgsConstructor
public class OrderController {

    private final OrderService orderService;

    @PostMapping
    public ResponseEntity createOrder(@RequestBody OrderDto orderDto) {
        val newOrder = Order.builder()
                .customer(orderDto.getCustomer())
                .barrels(orderDto.getBarrels())
                .type(orderDto.getType())
                .price(orderDto.getPrice())
                .build();

        Order order = orderService.createOrder(newOrder);

        OrderResponseDto orderResponseDto = OrderResponseDto.builder()
            .orderId(order.getOrderId())
                .type(order.getType())
                .price(order.getPrice())
                .build();
        return ResponseEntity.status(HttpStatus.CREATED).body(orderResponseDto);
    }

    @GetMapping("/{orderId}")
    public ResponseEntity getOrder(@PathVariable Integer orderId) {
        Order order = orderService.getOrderById(orderId);
        return ResponseEntity.status(HttpStatus.OK).body(order);
    }

}
